import React, { ReactNode } from "react";
import "./App.css";

class App extends React.Component {
  async componentDidMount(): Promise<void> {
    const response = await fetch("https://api-nba-v1.p.rapidapi.com/seasons/", {
      method: "GET",
      headers: {
        "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com",
        "X-RapidAPI-Key": "7e1be36919mshef8b4240f74c3adp191d91jsn50c0e299f6b4"
      }
    });
    const myJson = await response.json();
    console.log(myJson.api.seasons);
  }

  render(): ReactNode {
    return <header className="App-header">NBA application</header>;
  }
}

export default App;
